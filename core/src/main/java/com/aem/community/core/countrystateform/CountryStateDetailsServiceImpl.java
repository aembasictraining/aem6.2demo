package com.aem.community.core.countrystateform;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.jcr.Session;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Component(metatype = true, immediate = true, label = "Country State Details Service")
@Service(CountryStateDetailsService.class)
@Properties({
		@Property(name = "service.pid", value = "com.esri.sites.services.impl.CountryStateDetailsServiceImpl", propertyPrivate = false),
		@Property(name = "service.vendor", value = "NationalGeographic") })
public class CountryStateDetailsServiceImpl implements CountryStateDetailsService {

    private static final Logger log = LoggerFactory.getLogger(CountryStateDetailsServiceImpl.class);

    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    @Reference
    private SlingRepository slingRepository;

    ResourceResolver resourceResolver;
    Session session; 
    
    List<Country> countryList;
    Map<String,Country> countryStateMap;
    
    @Activate
    protected final void activate(ComponentContext ctx) {
	log.info("Activated CountryStateDetailsServiceImpl");
	
	Map<String,Object> paramMap = new HashMap<String,Object>();
        paramMap.put(ResourceResolverFactory.SUBSERVICE, "countryStateDetailsService");
        
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
            session = resourceResolver.adaptTo(Session.class);
            log.info("Got resourceresolver and session :: " + session.getUserID());
            
            updateCountryStates("/content/dam/nationalgeographic/config/countrylist.xml");
            log.info("Executed Update Country States from Activate");
        } catch (LoginException le) {
            log.error("Login Exception in Activate Method of CountryStateDetailsServiceImpl :: " + le.getMessage());
	} catch (Exception e) {
	    log.error("Exception in Activate Method of CountryStateDetailsServiceImpl :: " + e.getMessage());
	}
    }

    @Deactivate
    protected void deactivate(ComponentContext ctx) throws Exception {
	log.info("Deactivated CountryStateDetailsServiceImpl");
    }

    @Override
    public List<Country> getCountries() {		
	return countryList;
    }
    
    @Override
    public List<State> getStates(String countryCode) {		
	List<State> statesMap = new ArrayList<State>();
	try {
	    Country countryObject = countryStateMap.get(countryCode);
	    if(countryObject != null){
		statesMap = countryObject.getStateList();
	    }
	} catch (Exception e) {
	    log.error("Exception in CountryStateDetailsServiceImpl getStates method :: " + e.getMessage());
	}

	return statesMap;
    }
    
    @Override
    public void updateCountryStates(String filePath) {		
	try {
	    Resource assetResource = resourceResolver.getResource(filePath);
	    if((assetResource != null) && (assetResource instanceof Resource)){
		log.info("found resource");
		Resource fileResource = resourceResolver.getResource(filePath + "/jcr:content/renditions/original/jcr:content");
        	if((fileResource != null) && (fileResource instanceof Resource)){
        	    log.info("found rendition");
        	    ValueMap resourceProperties = fileResource.adaptTo(ValueMap.class);
        	    InputStream fileBinary = resourceProperties.get("jcr:data", InputStream.class);
        	    log.info("got inputstream");
        	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        	    DocumentBuilder builder;
        	    builder = factory.newDocumentBuilder();
        	    Document doc;
        	    doc = builder.parse(fileBinary);			
        	    NodeList countryNodes = doc.getElementsByTagName("country");      	     		
        	    
        	    countryList = new ArrayList<Country>();
        	    countryStateMap = new HashMap<String,Country>();
        	    
        	    for (int countryCount = 0; countryCount < countryNodes.getLength(); countryCount++) {
        		Country country = new Country();
        		    
        		NamedNodeMap countryAttributes = countryNodes.item(countryCount).getAttributes();
        		log.info("got country attributes :: " + countryAttributes.getLength());
        		for(int attrCount = 0 ; attrCount<countryAttributes.getLength() ; attrCount++) {
        		    Attr attribute = (Attr)countryAttributes.item(attrCount);     
        		    if(attribute.getName().equalsIgnoreCase("code")){
        			country.setCountryCode(attribute.getValue());
        			log.info("country code :: " + attribute.getValue());
        		    }
        		    if(attribute.getName().equalsIgnoreCase("text")){
        			country.setCountryName(attribute.getValue());
        			log.info("country text :: " + attribute.getValue());
        		    }
        		}
        		    
        		List<State> stateList = new ArrayList<State>();
        		
        		//Get States
        		NodeList statesNodes = countryNodes.item(countryCount).getChildNodes();
        		log.info("got states :: " + statesNodes.getLength());
        		for (int stateCount = 0; stateCount < statesNodes.getLength(); stateCount++) {
        		    if (statesNodes.item(stateCount).getNodeType() == Node.ELEMENT_NODE) {
                		    State state = new State();
                		    NamedNodeMap stateAttributes = statesNodes.item(stateCount).getAttributes();
                		    log.info("got state attributes :: " + stateAttributes.getLength());
                		    for(int attrCount = 0 ; attrCount<stateAttributes.getLength() ; attrCount++) {
                			Attr attribute = (Attr)stateAttributes.item(attrCount);     
                			if(attribute.getName().equalsIgnoreCase("code")){
                			    state.setStateCode(attribute.getValue());
                			    log.info("state code :: " + attribute.getValue());
                        		}
                			if(attribute.getName().equalsIgnoreCase("text")){
                			    state.setStateName(attribute.getValue());
                			    log.info("state text :: " + attribute.getValue());
                			}
                		    }
                		    
                		    stateList.add(state);
        		    } else {
        			log.info("whitespace node");
        		    }       		         		  
        		}
        		
        		country.setStateList(stateList);
        		countryList.add(country);
        		countryStateMap.put(country.getCountryCode(), country);
        	    }     		
        	}
	    }
	} catch(ParserConfigurationException pce){
	    log.error("ParserConfigurationException in updateCountryStates of CountryStateDetailsServiceImpl :: " + pce.getMessage());
	} catch (SAXException se){
	    log.error("SAXException in updateCountryStates of CountryStateDetailsServiceImpl :: " + se.getMessage());
	} catch (IOException ie){
	    log.error("IOException in updateCountryStates of CountryStateDetailsServiceImpl :: " + ie.getMessage());
	}
    }
}