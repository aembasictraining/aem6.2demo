package com.aem.community.core.models;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.cq.sightly.WCMUsePojo;
import com.aem.community.core.objects.CalendarEvents;
import com.aem.community.core.objects.Event;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CalendarEventsModel extends WCMUsePojo {
	
	private static final Logger log = LoggerFactory.getLogger(CalendarEventsModel.class);
	
	private Map<String,String> months;
	private Map<String,List<Event>> events;
	
	public Map<String,String> getMonths(){
		return months;
	}
	
	public Map<String,List<Event>> getEvents(){
		return events;
	}
	
	@Override
	public void activate() {
		String jsonFilePath = getProperties().get("jsonFilePath", "/content/dam/calendarevents.json");
		
		try {
			Resource assetResource = getResourceResolver().getResource(jsonFilePath);
		
			if((assetResource != null) && (assetResource instanceof Resource)){
				Resource fileResource = getResourceResolver().getResource(assetResource.getPath() + "/jcr:content/renditions/original/jcr:content");
	        	if((fileResource != null) && (fileResource instanceof Resource)){
	        	    ValueMap resourceProperties = fileResource.adaptTo(ValueMap.class);
	        	    InputStream fileBinary = resourceProperties.get("jcr:data", InputStream.class);
	        	    Reader reader = new InputStreamReader(fileBinary, "UTF-8");
	        	    Gson gson = new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create();
	        	    CalendarEvents calendarEvents  = gson.fromJson(reader, CalendarEvents.class);
	        	    months = calendarEvents.getMonths();
	        	    events = calendarEvents.getEvents();	        	  
	        	}
			}
		} catch(UnsupportedEncodingException ue) {
			log.error("UnsupportingEncodingException in activate() method of CalendarEventsModel :: " + ue.getMessage());
		} catch(Exception e) {
			log.error("Exception in activate() method of CalendarEventsModel :: " + e.getMessage());
		}
	}
}