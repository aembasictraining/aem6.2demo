package com.aem.community.core.objects;

import java.util.Date;

public class Event {
	//private Date date;
	private String dateDisplayText;
	private String title;
	private String titleBackgroundColor;
	private String description;
	private String image;
	private String addToCalendarText;
	private String addToCalendarLink;
	
	/*public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}*/
	public String getDateDisplayText() {
		return dateDisplayText;
	}
	public void setDateDisplayText(String dateDisplayText) {
		this.dateDisplayText = dateDisplayText;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitleBackgroundColor() {
		return titleBackgroundColor;
	}
	public void setTitleBackgroundColor(String titleBackgroundColor) {
		this.titleBackgroundColor = titleBackgroundColor;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getAddToCalendarText() {
		return addToCalendarText;
	}
	public void setAddToCalendarText(String addToCalendarText) {
		this.addToCalendarText = addToCalendarText;
	}
	public String getAddToCalendarLink() {
		return addToCalendarLink;
	}
	public void setAddToCalendarLink(String addToCalendarLink) {
		this.addToCalendarLink = addToCalendarLink;
	}
}
