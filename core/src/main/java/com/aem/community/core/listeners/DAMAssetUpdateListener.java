package com.aem.community.core.listeners;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(configurationFactory = true, label = "DAM Asset Update Listener", description = "DAM Asset Update SQS Notification", metatype = true, immediate = true, enabled = true)
@Service(value = EventHandler.class)
@Properties({ 
	@Property(name = EventConstants.EVENT_TOPIC, value = { SlingConstants.TOPIC_RESOURCE_CHANGED }, propertyPrivate = true),
    @Property(name = EventConstants.EVENT_FILTER, value = "(path=/content/dam/*)", propertyPrivate = true) 
})
public class DAMAssetUpdateListener implements EventHandler {

    private static final Logger log = LoggerFactory.getLogger(DAMAssetUpdateListener.class);
    
    @Override
    public void handleEvent(Event event) {
        log.info("EVENT TOPIC :: " + event.getTopic());
        log.info("EVENT PROPERTY :: " + event.getProperty(SlingConstants.PROPERTY_PATH));
    }
}