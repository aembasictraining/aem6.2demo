package com.aem.community.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.aem.community.core.objects.ProductPage;

@Model(adaptables=Resource.class)
public class ManipalDemoSMJavaModel {
	
	private static final Logger log = LoggerFactory.getLogger(ManipalDemoJavaModel.class);
	
	private ValueMap properties;
	
	@Inject @Default(values="Default Heading")
	private String heading;
	@Inject
	private String parentPath;
	
	private List<ProductPage> pageList;
	
	@Inject
	ResourceResolver resourceResolver;
	
	public String getHeading() {
		return heading;
	}
	
	public List<ProductPage> getPageList(){	
		Resource parentResource = resourceResolver.getResource(parentPath);
		
		if((parentResource != null) && (parentResource instanceof Resource)) {
			pageList = new ArrayList<ProductPage>();
			
			Iterable<Resource> childResources = parentResource.getChildren();
			for(Resource childResource : childResources) {
				if(!childResource.getName().equalsIgnoreCase("jcr:content")) {
					ValueMap resourceProperties = childResource.getValueMap();
					String title = resourceProperties.get("jcr:title","Default Page Title");
					String url = childResource.getPath();
					
					ProductPage productPage = new ProductPage();
					productPage.setTitle(title);
					productPage.setUrl(url);
					
					pageList.add(productPage);
				}
			}
		}
		
		return pageList;
	}
}
