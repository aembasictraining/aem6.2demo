package com.aem.community.core.countrystateform;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.commons.jcr.JcrConstants;

@Component(configurationFactory = true, label = "Country State Config Update Service", description = "Country State Config Update Service", metatype = true, immediate = true, enabled = true)
@Service(value = EventHandler.class)
@Properties({ @Property(name = EventConstants.EVENT_TOPIC, value = { SlingConstants.TOPIC_RESOURCE_CHANGED }, propertyPrivate = true),
        @Property(name = EventConstants.EVENT_FILTER, value = "(path=/content/dam/nationalgeographic/config/*)", propertyPrivate = true) })
public class CountryStateUpdateListener implements EventHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountryStateUpdateListener.class);
    
    @Reference
    CountryStateDetailsService countryStateDetailsService;
    
    @Override
    public void handleEvent(Event event) {
        String propPath = (String) event.getProperty(SlingConstants.PROPERTY_PATH);
        String nodePath = propPath.split("/" + JcrConstants.JCR_CONTENT).length > 0
                ? propPath.split("/" + JcrConstants.JCR_CONTENT)[0] : null;
        if (nodePath != null) {
            LOGGER.info("Country State Configuration Updated " + nodePath);
            countryStateDetailsService.updateCountryStates(nodePath);
        } else {
            LOGGER.info("Country State Configuration couldn't be updated");
        }
    }
}
