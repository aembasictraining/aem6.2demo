package com.aem.community.core.countrystateform;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/fetchcountrystate", methods = "GET", metatype=true)
@Properties({
        @Property(name = "service.pid", value = "com.esri.sites.servlets.FetchCountryStateServlet", propertyPrivate = false),
        @Property(name = "service.vendor", value = "NationalGeographic", propertyPrivate = false) })
public class FetchCountryStateServlet extends SlingSafeMethodsServlet {

    private static final Logger log = LoggerFactory.getLogger(FetchCountryStateServlet.class);
	
    @Reference
    CountryStateDetailsService countryStateDetailsService;
    
    @Override
    protected void doGet(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {
	
	String countryCode = "";
	
	if(req.getParameter("countryCode") != null){
	    countryCode = req.getParameter("countryCode");
	}
	
	List<State> stateList = countryStateDetailsService.getStates(countryCode);
	
	resp.setContentType("application/json");
    	PrintWriter out = resp.getWriter();
    	Gson gson = new Gson();
    	String jsonString = gson.toJson(stateList);    	
    	out.print(jsonString);
    	out.flush();
    }
}