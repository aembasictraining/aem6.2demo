package com.aem.community.core.countrystateform;

import java.util.List;

public class Country {
    private String countryCode;
    private String countryName;
    private List<State> stateList;
    
    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public String getCountryName() {
        return countryName;
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    public List<State> getStateList() {
        return stateList;
    }
    public void setStateList(List<State> stateList) {
        this.stateList = stateList;
    }
}
