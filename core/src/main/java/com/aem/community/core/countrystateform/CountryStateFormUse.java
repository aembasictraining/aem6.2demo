package com.aem.community.core.countrystateform;

import com.adobe.cq.sightly.WCMUsePojo;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CountryStateFormUse extends WCMUsePojo {
	
    private static final Logger log = LoggerFactory.getLogger(CountryStateFormUse.class);
    
    private List<Country> countries;
	
    @Override
    public void activate() {
	log.info("inside activate");
	CountryStateDetailsService service = getSlingScriptHelper().getService(CountryStateDetailsService.class);
	log.info("got service");
	countries = service.getCountries();
	log.info("got countries :: " + countries.size());
    }
    
    public List<Country> getCountries() {
        return countries;
    }
}