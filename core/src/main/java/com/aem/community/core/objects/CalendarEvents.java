package com.aem.community.core.objects;

import java.util.List;
import java.util.Map;

public class CalendarEvents {
	private Map<String,String> months;
	private Map<String,List<Event>> events;
	
	public Map<String, String> getMonths() {
		return months;
	}
	public void setMonths(Map<String, String> months) {
		this.months = months;
	}
	public Map<String,List<Event>> getEvents() {
		return events;
	}
	public void setEvents(Map<String,List<Event>> events) {
		this.events = events;
	}
}
