package com.aem.community.core.servlets;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component(immediate = true, metatype = true)
@Service(Servlet.class)
@Properties({ 
	@Property(name = "sling.servlet.resourceTypes", value = { "cc/components/templates/rss" }),
	@Property(name = "service.description", value = "RSS Feed Servlet"),
	@Property(name = "sling.servlet.extensions", value = { "html", ".xml"} ),
	@Property(name = "sling.servlet.methods", value = "GET") 
	})
public class RSSFeed extends SlingSafeMethodsServlet {

	Logger log = LoggerFactory.getLogger(RSSFeed.class);
	
	@Reference
    private QueryBuilder builder;
	
	public static final String FEED_TYPE = "rss_2.0";
	
	private static final long serialVersionUID = 1L;
	
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/xml; charset=UTF-8");
		
		try {
			ResourceResolver resourceResolver = request.getResourceResolver();
			Session session = resourceResolver.adaptTo(Session.class);
			Resource currentRssResource = request.getResource();
			
			SyndFeed feed = new SyndFeedImpl();
			
			if((currentRssResource != null) && (currentRssResource instanceof Resource)){
				//Fetch Title & Link from Parent RSS Page
				Resource parentRssResource = currentRssResource.getParent();
				if((parentRssResource != null) && (parentRssResource instanceof Resource)){
					Page parentRssPage = parentRssResource.adaptTo(Page.class);
					ValueMap parentRssPageProperties = parentRssPage.getProperties();
					
					feed.setTitle(parentRssPage.getTitle());
					feed.setLink(resourceResolver.map(parentRssPage.getPath()));
				}
				
				//Fetch rest of the configured properties from child RSS Page
				Page currentRssPage = currentRssResource.adaptTo(Page.class);
				ValueMap currentRssPageProperties = currentRssPage.getProperties();
				feed.setFeedType(FEED_TYPE);
				feed.setDescription(currentRssPageProperties.get("description", ""));
				
				int numEntries = currentRssPageProperties.get("items", 15);
				String [] tags = currentRssPageProperties.get("tags", String[].class);
				String [] contentTypes = currentRssPageProperties.get("contentTypes", String[].class);
				
				Map<String, String> queryMap = new HashMap<String, String>();
	
				queryMap.put("path", "/content");
				queryMap.put("type", "cq:Page");
				queryMap.put("orderby", "@ccDate");
				queryMap.put("orderby.sort", "desc");
				queryMap.put("orderby", "@pubdate");
				queryMap.put("orderby.sort", "desc");
				queryMap.put("orderby", "@jcr:content/cq:lastModified");
				queryMap.put("orderby.sort", "desc");
				
				int propertyCount = 1;
				
				for(int templateCount=1; templateCount<=tags.length; templateCount++) {
					queryMap.put("group."+propertyCount+"_property", "@jcr:content/cq:template");
					queryMap.put("group."+propertyCount+"_property.value", contentTypes[templateCount]);
					propertyCount++;
				}
				
				for(int tagCount=1; tagCount<=tags.length; tagCount++) {
					queryMap.put("group."+propertyCount+"_tagid.property", "@jcr:content/cq:tags");
					queryMap.put("group."+propertyCount+"_tagid", tags[tagCount]);
					propertyCount++;
				}
				 
				queryMap.put("p.offset", "0");
				queryMap.put("p.limit", String.valueOf(numEntries));
				                    
				//Create a Query instance
				Query query = builder.createQuery(PredicateGroup.create(queryMap), session);
				
				//Get the query results
				SearchResult result = query.getResult();
				                  
				for(Hit hit : result.getHits()) {
					String resultPath = hit.getPath();
	
				    Resource resultResource = resourceResolver.getResource(resultPath);
				    if((resultResource != null) && (resultResource instanceof Resource)) {
				    	Page page = resultResource.adaptTo(Page.class);
				    } 
				}
				
				feed.setPublishedDate(new Date());
			} 
		} catch(RepositoryException re) {
			log.error("Repository Exception in RSSFeed doGet Method :: " + re.getMessage());
		} catch(Exception e) {
			log.error("Exception in RSSFeed doGet Method :: " + e.getMessage());
		}
	}
}