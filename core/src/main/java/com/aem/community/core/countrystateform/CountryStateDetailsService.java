package com.aem.community.core.countrystateform;

import java.util.List;

public interface CountryStateDetailsService {
    List<Country> getCountries();
    List<State> getStates(String countryCode); 
    void updateCountryStates(String filePath);
}
